import bittrex
import pandas as pd
import plotly.offline as po
import plotly.graph_objs as go
import datetime as dt
import time

def get_candles(market, tick_interval):
    my_bittrex = bittrex.Bittrex(None, None, api_version=bittrex.API_V2_0)
    candles = my_bittrex.get_candles(market, tick_interval)
    result = candles['result']
    result = result[len(result) - 200:]
    df = pd.DataFrame(result)
    return df

def get_latest_candle(market, tick_interval):
    pass

def write_dataframe(dataframe, filename):
    dataframe.to_csv(filename)
    return

def read_dataframe(filename):
    df = pd.read_csv(filename)
    return df

def draw_candle(dataframe, title, filename):
    T = [dt.datetime.strptime(t, '%Y-%m-%dT%H:%M:%S') for t in dataframe['T']]
    trace = go.Candlestick(x = T, open=dataframe.O, high=dataframe.H, low=dataframe.L, close=dataframe.C)
    data = [trace]

    layout = go.Layout(title=title)
    fig = go.Figure(data=data, layout=layout)
    po.plot(fig, filename=filename)
    return

if __name__ == '__main__':
    #df = get_candles("USDT-NEO", bittrex.TICKINTERVAL_THIRTYMIN)
    #write_dataframe(df, 'USDT-NEO-30M-200.csv')

    #df = read_dataframe('USDT-NEO-30M-200.csv')
    #draw_candle(df, 'USDT-NEO-30M', 'test_plot.html')
    
    print dt.datetime.today()
    my_bittrex = bittrex.Bittrex(None, None, api_version=bittrex.API_V2_0)
    print dt.datetime.today()
    print my_bittrex.get_latest_candle('USDT-NEO', bittrex.TICKINTERVAL_FIVEMIN)
    print dt.datetime.today()
    print my_bittrex.get_latest_candle('USDT-NEO', bittrex.TICKINTERVAL_FIVEMIN)
    print dt.datetime.today()