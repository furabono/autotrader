import bittrex_test as bt
import plotly.offline as po
import plotly.graph_objs as go
import plotly.tools as pt
import analysis

if __name__ == '__main__':
    df = bt.read_dataframe('USDT-NEO-30M-200.csv')

    sma_5 = analysis.get_SMA(df['C'], 5)
    sma_20 = analysis.get_SMA(df['C'], 20)
    ema_5 = analysis.get_EMA(df['C'], 5)
    ema_20 = analysis.get_EMA(df['C'], 20)
    obv = analysis.get_OBV(df, columns=['C','V'])
    macd, macds, macdo = analysis.get_MACD(df['C'])
    K, D = analysis.get_Stochastic(df, columns=['H', 'L', 'C'])

    candle = go.Candlestick(open=df['O'], high=df['H'], low=df['L'], close=df['C'], xaxis='x', yaxis='y1', name='ohlc')
    volume = go.Bar(y=df['V'], xaxis='x', yaxis='y2', name='volume')
    go_sma_5 = go.Scatter(y=sma_5, xaxis='x', yaxis='y1', name='sma5')
    go_sma_20 = go.Scatter(y=sma_20, xaxis='x', yaxis='y1', name='sma20')
    go_ema_5 = go.Scatter(y=ema_5, xaxis='x', yaxis='y1', name='ema5')
    go_ema_20 = go.Scatter(y=ema_20, xaxis='x', yaxis='y1', name='ema20')
    go_obv = go.Scatter(y=obv, xaxis='x', yaxis='y3', name='obv')
    go_macd = go.Scatter(y=macd, xaxis='x', yaxis='y4', name='macd')
    go_macds = go.Scatter(y=macds, xaxis='x', yaxis='y4', name='macd_signal')
    go_macdo = go.Bar(y=macdo, xaxis='x', yaxis='y4', name='macd_oscilator')
    go_K = go.Scatter(y=K, xaxis='x', yaxis='y5', name='Stoch_%K')
    go_D = go.Scatter(y=D, xaxis='x', yaxis='y5', name='Stoch_%D')

    #lo_main = go.Layout(title='USDT-NEO-30M')
    #fig_main = go.Figure(data=[candle, go_sma_5, go_sma_20, go_ema_5, go_ema_20], layout=lo_main)

    #fig_vol = go.Figure(data=[volume])
    #fig_obv = go.Figure(data=[go_obv])
    #fig_macd = go.Figure(data=[go_macd, go_macds, go_macdo])
    #fig_stoch = go.Figure(data=[go_K, go_D])

    layout = go.Layout(
        yaxis1=dict(
            anchor='y1',
            domain=[0.6,1],
            autorange=True
        ),
        yaxis2=dict(
            anchor='y2',
            domain=[0.45,0.55],
            autorange=True
        ),
        yaxis3=dict(
            anchor='y3',
            domain=[0.3,0.4],
            autorange=True
        ),
        yaxis4=dict(
            anchor='y4',
            domain=[0.15,0.25],
            autorange=True,
            zeroline=True
        ),
        yaxis5=dict(
            anchor='y5',
            domain=[0,0.1],
            range=[0,100],
            zeroline=True
        ),
        title='USDT-NEO-30M',
        xaxis=dict(
            rangeslider=dict(
                visible=False
            )
        )
    )

    data=[candle, go_sma_5, go_sma_20, go_ema_5, go_ema_20, volume, go_obv, go_macd, go_macds, go_macdo, go_K, go_D]

    #fig = pt.make_subplots(rows=3, cols=1, shared_xaxes=True)
    #fig.append_trace(candle)
    fig = go.Figure(data=data, layout=layout)
    po.plot(fig, filename='plot.html')

    print macd