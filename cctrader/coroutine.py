import asyncio
import time
import sqlite3
import api

def periodic_task(interval: float, repeat: int):
    def decorator(func):
        async def wrapper(*args, **kwargs):
            for i in range(0, repeat - 1):
                begin_time = time.time()
                func(*args, **kwargs)
                await asyncio.sleep(interval - time.time() + begin_time)
            func(*args, **kwargs)
        return wrapper
    return decorator

class PeriodicTask():
    def __init__(interval: float, repeat: int):
        pass

@periodic_task(1, 10)
def test_func():
    print(time.time())

def main():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(test_func())
    loop.close()
    print('finish')
    t = test_func()

if __name__ == '__main__':
    main()