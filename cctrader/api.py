from collections import namedtuple
from typing import List
import requests
import time

OrderBook = namedtuple("OrderBook", ['timestamp', 'currency', 'bid', 'ask'])
Tick = namedtuple("Tick", ["timestamp", "price", "quantity"])

class Coinone():
    __slots__ = ['_token', '_secret', '_url']

    def __init__(self, token, secret):
        self._token = token
        self._secret = secret
        self._url = "https://api.coinone.co.kr/"

    def _get(self, sub_url: str, data: dict=None) -> dict:
        if not isinstance(sub_url, str) or not isinstance(data, dict):
            raise TypeError

        url = self._url + sub_url
        r = requests.get(url, data)
        j = r.json()

        if (j['result'] != 'success'):
            raise Exception('Http Request Failed')

        return j

    def get_order_book(self, currency: str) -> OrderBook:
        if not isinstance(currency, str):
            raise TypeError

        j = self._get('orderbook/', {'currency':currency})
        return OrderBook(j['timestamp'], j['currency'], j['bid'], j['ask'])

    def get_ticks(self, currency: str, period: int) -> List[Tick]:
        if not isinstance(currency, str) or not isinstance(period, int):
            raise TypeError

        j = self._get('trades/', {'currency':currency, 'period':'hour'})

        begin_time = int(j['timestamp']) - period
        ticks = []
        cos = j['completeOrders']
        cos.reverse()
        for t in cos:
            ts = int(t['timestamp'])

            if ts <= begin_time:
                break

            ticks.append(Tick(ts, int(t['price']), float(t['qty'])))
        
        ticks.reverse()
        return ticks

    def get_tick(self, currency :str) -> Tick:
        if not isinstance(currency, str):
            raise TypeError
        
        j = self._get('ticker', {'currency':currency})

        return Tick(j['timestamp'], j['last'], j[''])


def main():
    c = Coinone(None, None)

    for i in range(10):
        begin = time.time()
        ticks = c.get_ticks('btc', 3)
        print(time.time()-begin)
        time.sleep(1)

if __name__ == "__main__":
    main()