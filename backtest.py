import pandas as pd
import bittrex_test as bt
import plotly.offline as po
import plotly.graph_objs as go
import talib.abstract as tl

class Backtest():
    def __init__(self, cash=10000, strategy=None):
        self.cash = 10000
        self.strategy = strategy
        return

    def set_cash(self, cash):
        self.cash = cash
        return

    def set_strategy(self, strategy):
        self.strategy = strategy
        return

    def run(self, data):
        record = pd.DataFrame(columns=['cash', 'stock', 'net', '%net'])
        data = data.iloc[::-1]
        row, col = data.shape

        context = Context(self.cash)
        for i in range(0, row):
            context = Context(context.cash, context.stock, data.tail(i+1).reset_index(), context.record)
            self.strategy(context)
            net = (context.cash+context.stock*context.data.C[0]) - self.cash
            pnet = net * 100.0 / self.cash
            record.loc[i] = [context.cash, context.stock, net, pnet]

        return record


class Context():
    def __init__(self, cash, stock=0.0, data=None, record={}):
        self.data = data
        self.cash = cash
        self.stock = stock
        self.record = record
        self.order = []
        return

    def buy(self, stock, price):
        if (stock*price <= self.cash):
            self.cash -= stock*price
            self.stock += stock
            self.order.append((stock, price))
        return

    def sell(self, stock, price):
        if self.stock >= stock:
            self.cash += stock*price
            self.stock -= stock
            self.order.append((-stock, price))
        return

def test_strategy(context):
    data = context.data
    record = context.record
    i,_ = data.shape
    if i < 20: return

    price = data.C[0]
    ma5 = data['C'].head(5).mean()
    ma20 = data['C'].head(20).mean()

    if context.stock > 0:
        bought = context.record['bought']
        margin = price - bought
        last_margin = context.record['last_margin']

        if margin < last_margin * 0.5:
            context.sell(context.stock, price)
        else:
            context.record['last_margin'] = margin

    elif record.has_key('last_ma5') and record.has_key('last_ma20'):
        last_ma5 = record['last_ma5']
        last_ma20 = record['last_ma20']

        if ma5>ma20 and last_ma5<last_ma20:
            price = context.data.C[0]
            stock = context.cash / price
            context.buy(stock, price)
            context.record['bought'] = price
            context.record['last_margin'] = 0

    record['last_ma5'] = ma5
    record['last_ma20'] = ma20
    return

def main():
    df = bt.read_dataframe('USDT-NEO-30M-200.csv')
    backtest = Backtest(10000, test_strategy)
    record = backtest.run(df)

    close = go.Scatter(y=df['C'], name='Close')
    ma5 = go.Scatter(y=df['C'].rolling(window=5).mean(), name='MA5')
    ma20 = go.Scatter(y=df['C'].rolling(window=20).mean(), name='MA20')

    layout = go.Layout(title='MA')
    fig = go.Figure(data=[close, ma5, ma20], layout=layout)
    po.plot(fig, filename='MA_plot.html')

    record.to_html('record.html')
    return

if __name__ == '__main__':
    df = bt.read_dataframe('USDT-NEO-30M-200.csv')
    macd = tl.MACD(df.head(26), price='C')
    print macd

    #print macd, '\n', macds, '\n', macdo
    
    #print record