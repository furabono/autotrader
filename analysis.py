#coding:utf-8
import pandas as pd

def get_SMA(series, period):
    if not isinstance(series, pd.Series):
        raise Exception('series must be Pandas Series')

    #series = pd.Series()
    return series.rolling(window=period).mean()

def get_EMA(series, period):
    """
    EMA[now] = alpha * price[now] + (1-alpha) * EMA[previous]
    alpha = 2 / (1 + period)
    """
    if not isinstance(series, pd.Series):
        raise Exception('series must be Pandas Series')

    #series = pd.Series()
    return series.ewm(span=period).mean()

def get_MACD(series, short_period=12, long_period=26, signal_period=9):
    """
    MACD = EMA_SHORT - EMA_LONG\n
    SIGNAL = EMA_SIGNAL(MACD)\n
    OSCILATOR = MACD - SIGNAL
    """
    if not isinstance(series, pd.Series):
        raise Exception('series must be Pandas Series')

    #series = pd.Series()
    ema_short = get_EMA(series, short_period)
    ema_long = get_EMA(series, long_period)

    macd = ema_short - ema_long
    signal = get_EMA(macd, signal_period)
    oscilator = macd - signal

    return macd, signal, oscilator

def get_OBV(dataframe, columns=['Close','Volume']):
    """
    Close[now] > Close[previous] => OBV[previous] + Volume[now]\n
    Close[now] = Close[previous] => OBV[previous]\n
    Close[now] < Close[previous] => OBV[previous] - Volume[now]
    """
    if not isinstance(dataframe, pd.DataFrame):
        raise Exception('dataframe must be Pandas DataFrame')

    #dataframe = pd.DataFrame()
    close = dataframe[columns[0]]
    volume = dataframe[columns[1]]
    row, col = dataframe.shape
    obv = [0]
    for i in range(1, row):
        if close[i] > close[i-1]:
            obv.append(obv[i-1] + volume[i])
        elif close[i] < close[i-1]:
            obv.append(obv[i-1] - volume[i])
        else:
            obv.append(obv[i-1])

    return pd.Series(obv)

def get_Stochastic(dataframe, columns=['High', 'Low', 'Close'], period=14):
    """
    %K = (Close[now] - Min(Low)) / (Max(High) - Min(Low)) * 100
    %D = SMA_3(%K)
    """
    if not isinstance(dataframe, pd.DataFrame):
        raise Exception('dataframe must be Pandas DataFrame')

    high = dataframe[columns[0]]
    low = dataframe[columns[1]]
    close = dataframe[columns[2]]

    highest_high = high.rolling(window=period).max()
    lowest_low = low.rolling(window=period).min()
    
    K = (close - lowest_low) / (highest_high - lowest_low) * 100
    D = get_SMA(K, 3)
    return K, D